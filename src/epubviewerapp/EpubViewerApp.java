/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package epubviewerapp;

import java.io.ByteArrayInputStream;
import nl.siegmann.epublib.domain.Book;


import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import nl.siegmann.epublib.epub.EpubReader;

/**
 *
 * @author hehe
 */
public class EpubViewerApp extends Application {
    
    final Menu mnuFile = new Menu("File");
    final Menu mnuAbout = new Menu("About");

    final MenuItem mnuitemOpen = new MenuItem("Open");
    final MenuItem mnuitemExit = new MenuItem("Exit");
    
    File file;
        
    @Override
    public void start(final Stage primaryStage) {
        primaryStage.setTitle("Epub Viewer");
        Group root = new Group();
   
        Scene scene = new Scene(root, 400, 300);
        scene.setFill(Color.WHITE);    
        
        BorderPane borderPane = new BorderPane();
        // root.getChildren().add(btn);
           
        MenuBar mnuBar = new MenuBar();
        mnuBar.getMenus().addAll(mnuFile,mnuAbout);
        mnuFile.getItems().addAll(mnuitemOpen,mnuitemExit);
        
        final Label labelFile=new Label("Hello");
        labelFile.setFont(new Font("Verdana", 12));
        
        mnuitemOpen.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open Resource File");
                    
                if(file!=null){
                    File directory=file.getParentFile();
                    fileChooser.setInitialDirectory(directory);
                }
                FileChooser.ExtensionFilter extsionFilter=new FileChooser.ExtensionFilter("EPUB files(*.epub)","*.epub");
                fileChooser.getExtensionFilters().add(extsionFilter);
                    
                file=fileChooser.showOpenDialog(primaryStage);
                labelFile.setText(file.getPath());
                primaryStage.setTitle(file.getPath());
                
                try {
                    //Book eBook = new Book();
                    String path = file.getPath();
                    
                    Book readBook = new EpubReader().readEpub(new ByteArrayInputStream(path));
                }
                catch (Exception e){
                
                }
                
            }
        });
        
        EventHandler<ActionEvent> action = changeTabPlacement();
        mnuitemExit.setOnAction(action);
        //((BorderPane) scene.getRoot()).getChildren().addAll(mnuBar);
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        borderPane.setTop(mnuBar);
        borderPane.setCenter(labelFile);
        
        root.getChildren().add(borderPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    private EventHandler<ActionEvent> changeTabPlacement() {
        return new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MenuItem mnuItem = (MenuItem) event.getSource();
                String side = mnuItem.getText();
                
                if ("Open".equalsIgnoreCase(side)) {
                    System.out.println("Open File Selector");
                } else if ("Exit".equalsIgnoreCase(side)) {
                    System.exit(0);
                }
            }
        };
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
 
}
